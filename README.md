## Server Install Scripts.

```bash
BRANCH=master
mkdir /tmp/server-install
cd /tmp/server-install
curl -s https://bitbucket.org/gramgersh/server-install/get/$BRANCH.tar.gz | tar -xzf - --strip-components=1
./bin/setup-server.sh -s {serverspec}
```
