all:

clean:
	find . \( -name '*~' -o -name '#*' -o -name '.*.swp' -o -name ',*' \) -print0 | xargs -0 /bin/rm -f

check:
	for i in servers/*.serverspec modules/*.sh etc/*.sh ; do \
		/bin/bash -n $$i || exit 1 ; \
	done
