#!/bin/bash
#
#
PATH=/usr/local/sbin:/usr/localbin:/usr/sbin:/usr/bin:/sbin:/bin

INSTALLER_PROG="$1"
echo "##"
echo "## Starting $INSTALLER_PROG"
echo "##"

# This is an empty function that can be overridden in the master
# script for a cleanup at the end of the script.
function cleanup {
        :
}

# This is our internal cleanup script which runs the caller's
# cleanup() function, then removes temp files.
function _installer_internal_cleanup {
    cleanup
    /bin/rm -f "$INSTALLER_ALLTEMPFILES"
	echo "##"
	echo "## Finished $INSTALLER_PROG"
	echo "##"
	echo ""
}
trap _installer_internal_cleanup 0

# create a new temp file and add it to our INSTALLER_ALLTEMPFILES so that it's
# removed at the end of the script.
function newtempfile {
    typeset F
    F=$(mktemp /tmp/XXXXXXXX)
    INSTALLER_ALLTEMPFILES="$INSTALLER_ALLTEMPFILES $F"
    if [[ -n "$1" ]]; then
        eval "${1}=$F"
    else
        echo $F
    fi
}
INSTALLER_ALLTEMPFILES=""

function title {
	echo "# $@"
}

function errexit {
	xval="$1"
	shift
	if [[ $# -gt 0 ]]; then
		echo "$@"
	else
		echo "error in previous operation."
	fi
	exit $xval
}

function check4rpm {
	title "Checking for RPM $1"
	rpm -q "$1" >/dev/null
}

function chkinstallrpms {
	typeset _cir_r
	typeset _cir_toinstall
	typeset _cir_toupdate
	_cir_toinstall=()
	_cir_toupdate=()
	for _cir_r in "$@" ; do
		if check4rpm "$_cir_r" ; then
			_cir_toupdate+=($_cir_r)
		else
			_cir_toinstall+=($_cir_r)
		fi
	done
	if [[ "${#_cir_toupdate[@]}" -gt 0 ]]; then
		title "Updating ${_cir_toupdate[@]} package(s)"
		yum -y -q update "${_cir_toupdate[@]}"
	fi
	if [[ "${#_cir_toinstall[@]}" -gt 0 ]]; then
		title "Installing ${_cir_toinstall[@]} package(s)"
		yum -y -q install "${_cir_toinstall[@]}" || return 1
	fi
	return 0
}

# Generate a random password of the length of $1 (12 is default)
function getpass {
	__gplen=${1:12}
	cat /dev/urandom | tr -d -c 'a-zA-Z0-9' | fold -$__gplen | head -1
}

function yumupdate {
	title "Running a yum update.  This may take awhile ..."
	yum -y -q "$@" update
}
