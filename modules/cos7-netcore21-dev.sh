#!/bin/bash
#
# This sets up the cos7-netcore 2.1 environment
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

title "Checking for microsoft repo"
if ! check4rpm packages-microsoft-prod ; then
	rpm --import https://packages.microsoft.com/keys/microsoft.asc
	yum -y -q install https://packages.microsoft.com/config/rhel/7/packages-microsoft-prod.rpm || \
		errexit 1 "unable to install microsoft repo"
	yumupdate
fi

if ! check4rpm aspnetcore-runtime-2.1 ; then
	title "Installing keychain"
	yum -y -q install aspnetcore-runtime-2.1 dotnet-sdk-2.1
fi

# Check for Visual Studio repo
if [[ ! -f /etc/yum.repos.d/vscode.repo ]];then
	cat > /etc/yum.repos.d/vscode.repo <<EOF
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
EOF
fi

chk4rpm code
