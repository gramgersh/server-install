#!/bin/bash
#
# This grabs the cyberpower-client software from the git repo.
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

cd /usr/local || exit
title "Retrieving cyberpower-client software."
curl -s https://bitbucket.org/gramgersh/cyberpower-client/get/master.tar.gz | \
    tar xzf - --wildcards --strip-components=1 \
    --backup --suffix=.$(date "+%Y%m%d-%H%M") */bin */etc
