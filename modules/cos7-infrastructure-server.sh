#!/bin/bash
#
. ../etc/module-helpers.sh $(basename $0) || exit 1
#
title "Installing Infrastructure Server.  This may take awhile ..."
yum -q -y groupinstall "Infrastructure Server"

chkinstallrpms nfs-utils nfs4-acl-tools samba-client screen whois
