#!/bin/bash
#
. ../etc/module-helpers.sh $(basename $0) || exit 1
#
title "Installing Development Tools.  This may take awhile ..."
yum -q -y groupinstall "Development Tools"
chkinstallrpms emacs
