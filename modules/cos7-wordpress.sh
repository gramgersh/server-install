#!/bin/bash
#
# This performs the wordpress installation, assuming:
#
#  1. nginx
#  2. the root directory is /var/www/${WP_DOMAIN}
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

HTMLTOP=/var/www
SITESTOP=/etc/nginx/sites-available

TMPFILE=""
function cleanup {
	/bin/rm -f $TMPFILE
}
trap cleanup 0
TMPFILE=$(mktemp /tmp/XXXXXXXX)

# Make sure nginx and mariadb are installed
check4rpm nginx || errexit "nginx package not found"
check4rpm mariadb || errexit "mariadb package not found"

# This one isn't mandatory
chkinstallrpms certbot

#
# We're going to make nginx look more like the Ubuntu variant.  We
# will have the sites-available directory, then they need to be linked
# in to conf.d
#
title "Checking for nginx sites directories."
test -d $SITESTOP || mkdir -p $SITESTOP || exit 1

#
# Set up the environment for automatic install
# https://peteris.rocks/blog/unattended-installation-of-wordpress-on-ubuntu-server/

title "Enter information for creating the site."
read -t 30 -p "Enter domain/site name [empty or timeout to abort]> " WP_DOMAIN
if [[ -z "$WP_DOMAIN" ]] ; then
	echo ""
	echo "No domain/site name provided."
	exit 1
fi

WP_PATH="$HTMLTOP/$WP_DOMAIN"

# we're done
if [[ -d "$WP_PATH" ]]; then
	echo "$WP_PATH" already exists
	exit 0
fi

WP_ADMIN_USERNAME="wpadmin"
WP_ADMIN_PASSWORD=$(getpass 16)
WP_ADMIN_EMAIL="wpadmin@${WP_DOMAIN}"
WP_DB_DBNAME=$(echo $WP_DOMAIN | sed -e 's/\..*//g' | fold -16 | head -1)
WP_DB_USERNAME=$(echo $WP_DOMAIN | sed -e 's/\..*//g' | fold -16 | head -1)
WP_DB_PASSWORD=$(getpass 64)
#
# This is going to be the document root.
mkdir -p "$WP_PATH/public" "$WP_PATH"/logs

title "Creating database"
mysql -e "CREATE DATABASE $WP_DB_DBNAME;" || exit 1
mysql -e "GRANT ALL PRIVILEGES ON $WP_DB_DBNAME.* TO '$WP_DB_USERNAME'@'localhost' IDENTIFIED BY '$WP_DB_PASSWORD';" || exit 1
mysql -e "FLUSH PRIVILEGES;" || exit 1

#
# Install wordpress
title "Downloading and installing wordpress"
curl -s https://wordpress.org/latest.tar.gz | \
	tar -C $WP_PATH/public -xzf - --strip-components=1 || exit 1

title "Creating wp-config.php"
sed -e "s/username_here/$WP_DB_USERNAME/g" \
	-e "s/password_here/$WP_DB_PASSWORD/g" \
	-e "s/database_name_here/$WP_DB_DBNAME/g" \
	$WP_PATH/public/wp-config-sample.php > $WP_PATH/wp-config.php

# Replace all of the keys.
#
# Grab a file from the wordpress api so we don't need to worry about
# so much entropy.
curl -s https://api.wordpress.org/secret-key/1.1/salt/ | \
	awk '{ print $2 }' | sed -e "s/^'//g" -e "s/...$//g" > $TMPFILE
curl -s https://api.wordpress.org/secret-key/1.1/salt/ | \
	awk '{ print $2 }' | sed -e "s/^'//g" -e "s/...$//g" >> $TMPFILE

str="put your unique phrase here"
while grep "$str" $WP_PATH/wp-config.php >/dev/null; do
	read p < $TMPFILE
	sed -i -e 1d $TMPFILE
	sed -i -e "0,/$str/s/$str/$p/" $WP_PATH/wp-config.php;
done

# Allow the nginx user to write files here.
chown -R nginx:nginx $WP_PATH
chmod 640 $WP_PATH/wp-config.php

# Update the nginx sites.
title "Added sites to $SITESTOP"
echo "
server {
  listen 80;
  server_name $WP_DOMAIN www.$WP_DOMAIN;

  root $WP_PATH/public;
  index index.php;

  access_log $WP_PATH/logs/access.log;
  error_log $WP_PATH/logs/error.log;

  location / {
    try_files \$uri \$uri/ /index.php?\$args;
  }

  include /etc/nginx/default.d/php.conf;
}
" > $SITESTOP/$WP_DOMAIN.conf
echo "
server {
  listen 80;
  server_name $WP_DOMAIN www.$WP_DOMAIN;
  return 301 https://\$server_name\$request_uri;
}

server {
  listen 443 ssl http2;
  server_name $WP_DOMAIN www.$WP_DOMAIN;

  root $WP_PATH/public;
  index index.php;

  access_log $WP_PATH/logs/access.log;
  error_log $WP_PATH/logs/error.log;

  ssl_certificate           /etc/letsencrypt/live/$WP_DOMAIN/fullchain.pem;
  ssl_certificate_key       /etc/letsencrypt/live/$WP_DOMAIN/privkey.pem;
  ssl_trusted_certificate   /etc/letsencrypt/live/$WP_DOMAIN/chain.pem;
  ssl_dhparam               /etc/letsencrypt/live/$WP_DOMAIN/dhparam.pem;

  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
  ssl_prefer_server_ciphers on;
  ssl_session_timeout 1d;
  ssl_session_cache shared:SSL:50m;
  ssl_session_tickets off;
  ssl_stapling on;
  ssl_stapling_verify on;

  add_header Strict-Transport-Security max-age=15552000;

  location / {
    try_files \$uri \$uri/ /index.php?\$args;
  }

  include /etc/nginx/default.d/php.conf;
}
" > $SITESTOP/$WP_DOMAIN.ssl.conf

ln -s $SITESTOP/$WP_DOMAIN.conf /etc/nginx/conf.d/$WP_DOMAIN.conf
systemctl restart nginx

title "Registering user on local wordpress site."
DATE=$(date +%s)
HOSTLINE="127.0.0.1 $WP_DOMAIN www.$WP_DOMAIN # $DATE"
echo "$HOSTLINE" >> /etc/hosts
curl -s "http://www.$WP_DOMAIN/wp-admin/install.php?step=2" \
  --data-urlencode "weblog_title=$WP_DOMAIN"\
  --data-urlencode "user_name=$WP_ADMIN_USERNAME" \
  --data-urlencode "admin_email=$WP_ADMIN_EMAIL" \
  --data-urlencode "admin_password=$WP_ADMIN_PASSWORD" \
  --data-urlencode "admin_password2=$WP_ADMIN_PASSWORD" \
  --data-urlencode "pw_weak=1" | grep -i "<h1>"
sed -i -e "/$HOSTLINE/d" /etc/hosts

title "Saving wpadmin details"
touch $HOME/wp_$WP_DOMAIN.creds
chmod 600 $HOME/wp_$WP_DOMAIN.creds
cat >> $HOME/wp_$WP_DOMAIN.creds <<EOF
user_name=$WP_ADMIN_USERNAME
admin_email=$WP_ADMIN_EMAIL
admin_password=$WP_ADMIN_PASSWORD
EOF
