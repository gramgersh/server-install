#!/bin/bash
#
# This performs the basic server setup
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

chkinstallrpms epel-release nginx

if ! check4rpm remi-release ; then
	title "Installing and enabling remi repo"
	yum -y -q install http://rpms.remirepo.net/enterprise/remi-release-7.rpm || \
		errexit 1 "unable to install remi repo"
	COUNT=$(yum-config-manager --enable remi-php72 | wc -l)
	(( COUNT <= 1 )) && errxit 1 "Unable to enable remi repo."
fi

yumupdate

chkinstallrpms php php-common php-opcache php-pecl-mcrypt php-cli php-gd php-mysqlnd php-fpm php-mbstring

title "Verifying php version"
PHPVERSION=$(php --version | awk '{print $2;exit}')
echo "PHP version is $PHPVERSION"
case "$PHPVERSION" in
	7.2* ) : ;;
	* ) errexit 1 "PHP version is $PHPVERSION" ;;
esac

title "Retriving nginx user and group"
NGINXUSER=$(egrep '^user ' /etc/nginx/nginx.conf | sed -n -e 's/^user[ ]*//g' -e 's/;.*//p')
if [[ -z "$NGINXUSER" ]]; then
	errexit 1 "Cannot find user in /etc/nginx/nginx.conf file"
fi

NGINXGROUP=$(egrep '^group ' /etc/nginx/nginx.conf | sed -n -e 's/^group[ ]*//g' -e 's/;.*//p')
if [[ -z "$NGINXGROUP" ]]; then
	# Get the group name from the password entry
	NGINXGID=$(getent passwd $NGINXUSER | awk -F: '{ print $4 }')
	NGINXGROUP=$(getent group $NGINXGID | awk -F: '{ print $1 }')
fi

title "Updating /etc/php-fpm.d/www.conf file."
sed -i.orig -e "s/^user = .*/user = $NGINXUSER/" \
	-e "s/^group = .*/group = $NGINXGROUP/" \
	-e "s|^listen = .*|listen = /run/php-fpm/www.sock|" \
	-e "s|^;*listen.owner = .*|listen.owner = $NGINXUSER|" \
	-e "s|^;*listen.group = .*|listen.group = $NGINXGROUP|" \
	/etc/php-fpm.d/www.conf

DST=/etc/nginx/default.d/php.conf
if [[ ! -f $DST ]]; then
	title "Updating nginx configuration for php."
	echo '
location ~ \.php$
{
        try_files $uri =404;
        fastcgi_pass unix:/run/php-fpm/www.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
}
' > $DST
fi

title "Updating firewall"
firewall-cmd --zone=public --add-service http --permanent
firewall-cmd --zone=public --add-service https --permanent
firewall-cmd --reload

title "Enabling php-fpm and restarting nginx"
systemctl enable php-fpm
systemctl start php-fpm
systemctl enable nginx
systemctl restart nginx
