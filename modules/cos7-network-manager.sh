#!/bin/bash
# This performs the network setup
#
. ../etc/module-helpers.sh $(basename $0) || exit 1
#
# Check for networking
if ! type -p nmcli >/dev/null ; then
	exit
fi
title "Setting up network"
NMDEV=$(nmcli -t con show | awk -F: '{ print $4; exit(0) }')
VAR=""
VAL=""
if [[ -z "$NMDEV" ]]; then
	echo "Network-manager not installed"
	exit 0
fi

if [[ -n "$NETWORK_HOSTNAME" ]] ; then
	VAL=$(nmcli general hostname)
	if [[ "$VAL" != "$NETWORK_HOSTNAME" ]]; then
		title "Updating hostname"
		nmcli general hostname "$NETWORK_HOSTNAME"
	fi
fi

if [[ ${#NETWORK_IPADDRS[@]} -gt 0 ]] ; then
	title "Updating IPV4 addresses"
	nmcli con mod "$NMDEV" ipv4.method manual
	for ip in ${NETWORK_IPADDRS[@]} ; do
		nmcli con mod "$NMDEV" +ipv4.addresses "$ip"
	done
fi
if [[ -n "$NETWORK_NETMASK" ]] ; then
	nmcli con mod "$NMDEV" ipv4.netmask "$NETWORK_NETMASK"
fi
if [[ -n "$NETWORK_GATEWAY" ]] ; then
	nmcli con mod "$NMDEV" ipv4.gateway "$NETWORK_GATEWAY"
fi
if [[ ${#NETWORK_DNS[@]} -gt 0 ]] ; then
	for d in ${NETWORK_DNS[@]} ; do
		nmcli con mod "$NMDEV" +ipv4.dns $d
	done
fi

if [[ ${#NETWORK_IPV6ADDRS[@]} -gt 0 ]] ; then
	title "Updating IPV6 addresses"
	nmcli con mod "$NMDEV" ipv6.method manual
	for ip in ${NETWORK_IPADDRS[@]} ; do
		nmcli con mod "$NMDEV" +ipv6.addresses "$ip"
	done
fi
if [[ -n "$NETWORK_GATEWAY" ]] ; then
	nmcli con mod "$NMDEV" ipv6.gateway "$NETWORK_IPV6_DEFAULTGW"
fi
