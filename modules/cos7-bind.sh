#!/bin/bash
#
. ../etc/module-helpers.sh $(basename $0) || exit 1
#
chkinstallrpms bind bind-utils
title "Checking firewall ports."
if systemctl is-enabled firewalld.service >/dev/null ; then
	if ! firewall-cmd --list-services | grep -w dns >/dev/null ; then
		# add it now
		firewall-cmd --add-service=dns
		# add it forever
		firewall-cmd --add-service=dns --permanent
	fi
fi
title "Enabling and starting named"
if ! systemctl is-enabled named.service >/dev/null ; then
	systemctl enable named.service
fi
if ! systemctl is-active named.service >/dev/null ; then
	systemctl start named.service
fi
