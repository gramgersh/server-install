#!/bin/bash
#
# This performs the basic server setup
#
. ../etc/module-helpers.sh $(basename $0) || exit 1
#

yumupdate

title "Installing basic utils"
chkinstallrpms yum-cron yum-utils


PKGS=()
PKGS+=(augeas)
PKGS+=(curl)
PKGS+=(rdist)
PKGS+=(rsync)
PKGS+=(vim-X11)
PKGS+=(virt-what)
PKGS+=(wget)
PKGS+=(xorg-x11-apps)
PKGS+=(xorg-x11-fonts-ISO8859-1-100dpi)
PKGS+=(xorg-x11-fonts-ISO8859-1-75dpi)
PKGS+=(xorg-x11-fonts-Type1)
PKGS+=(xorg-x11-fonts-misc)
PKGS+=(xorg-x11-xauth)
PKGS+=(xterm)

chkinstallrpms "${PKGS[@]}"

yum groups mark convert

VIRTWHAT=$(virt-what 2>/dev/null)
if [[ "$VIRTWHAT" = "vmware" ]]; then
	PKGS+=(open-vm-tools)
fi

title "Checking for boot mode 3"
BOOTMODE=$(systemctl get-default)
if [[ "$BOOTMODE" != "multi-user.target" ]]; then
	systemctl set-default multi-user.target
fi

title "Checking grub to boot in text mode"
if grep 'rhgb quiet' /etc/default/grub >/dev/null 2>&1 ; then
#	sed -i -e 's/rhgb quiet/text quiet video=640x480/' /etc/default/grub
	sed -i -e 's/rhgb quiet/text quiet nomodeset/' /etc/default/grub
	grub2-mkconfig -o /boot/grub2/grub.cfg
fi

title "Checking for SELinux disabled"
SELINUXSTATUS=$(awk -F= '$1 == "SELINUX" { print $2; exit(0); }' /etc/sysconfig/selinux)
if [[ "$SELINUXSTATUS" != "disabled" ]] ; then
	augtool --autosave -b <<EOF
set /files/etc/sysconfig/selinux/SELINUX disabled
EOF
fi

title "Setting up bash startup files"
curl -s https://bitbucket.org/gramgersh/server-dotfiles/get/master.tar.gz | \
    tar xzf - --strip-components=2 --suffix=.$(date "+%Y%m%d-%H%M") */home

yumupdate
