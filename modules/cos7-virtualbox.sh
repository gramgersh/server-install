#!/bin/bash
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

chkinstallrpms kernel-devel kernel-headers make patch gcc

title "Checking for VirtualBox repo"
if ! yum -q repolist | grep virtualbox >/dev/null 2>&1 ; then
	yum-config-manager \
		--add-repo \
		https://download.virtualbox.org/virtualbox/rpm/el/virtualbox.repo || return 1
	rpm --import https://www.virtualbox.org/download/oracle_vbox.asc || return 1
fi
if ! echo y | yum -q repolist enabled | grep virtualbox >/dev/null 2>&1 ; then
	# If it's not enabled, enable the stable repo
	yum-config-manager --enable virtualbox >/dev/null
fi

if ! check4rpm VirtualBox-5.2 ; then
	title "Installing VirtualBox-5.2 and extension pack.  This may take awhile"
	yum -q -y install VirtualBox-5.2
	newtempfile VBOXOUTPUT
	wget -q https://download.virtualbox.org/virtualbox/5.2.22/Oracle_VM_VirtualBox_Extension_Pack-5.2.22.vbox-extpack
	echo "y" | VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-5.2.22.vbox-extpack > $VBOXOUTPUT
	tail -1 $VBOXOUTPUT
fi
