#!/bin/bash
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

if ! check4rpm vagrant ; then
	title "Installing vagrant"
	yum -q -y install https://releases.hashicorp.com/vagrant/2.2.4/vagrant_2.2.4_x86_64.rpm
fi
