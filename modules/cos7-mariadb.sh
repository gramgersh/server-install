#!/bin/bash
#
# This performs the basic server setup
#
. ../etc/module-helpers.sh $(basename $0) || exit 1
newtempfile TMPFILE

if ! check4rpm mariadb-server ; then
	chkinstallrpms mariadb-server
	title "Enabling and starting mariadb"
	systemctl enable mariadb
	systemctl start mariadb

	title "Securing the mariadb server."

	# Create a random password
	NEWPASS=$(getpass 12)
	# Create a temp my.cnf
	chmod 600 $TMPFILE
	cat > $TMPFILE <<EOF
# This was created by the cos7-mariadb.sh script.
[mysql]
use=root
password=$NEWPASS
EOF
	#
    mysql -e "UPDATE mysql.user SET Password=PASSWORD('$NEWPASS') WHERE User='root';"
	mysql -e "DROP DATABASE IF EXISTS test;"
	mysql -e "DELETE FROM mysql.user WHERE User='';"
	mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
	mysql -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%';"
	mysql -e "FLUSH PRIVILEGES;"

	if [[ ! -f $HOME/.my.cnf ]]; then
		cp $TMPFILE $HOME/.my.cnf
	else
		sed -e 's/^/#/' $TMPFILE >> $HOME/.my.cnf
	fi
fi
