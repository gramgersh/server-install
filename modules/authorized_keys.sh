#!/bin/bash
#
# This grabs the authorized_keys file from the git repo.
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

cd $HOME || exit
if [[ ! -d .ssh ]]  ; then
	mkdir .ssh
	chmod 700 .ssh
fi
cd .ssh || exit 1
TIMESTAMP=$(date +%Y%m%d%H%M)
DST=authorized_keys
BKUP=$DST.$TIMESTAMP
if [[ -f $DST ]]; then
	/bin/mv $DST $BKUP || exit 1
fi
title "Retrieving authorized_keys."
curl -s https://bitbucket.org/gramgersh/server-authorized_keys/get/master.tar.gz | tar xzf - --strip-components=1 */authorized_keys || exit 1
chmod 600 $DST
if [[ -f $BKUP ]]; then
	cat $BKUP >> $DST && /bin/rm $BKUP
fi
