#!/bin/bash
#
. ../etc/module-helpers.sh $(basename $0) || exit 1
#
title "Installing rpm prerequisites"
chkinstallrpms yum-utils device-mapper-persistent-data lvm2

title "Checking for docker repo"
if ! yum -q repolist | grep 'Docker CE' >/dev/null 2>&1 ; then
	yum-config-manager \
		--add-repo \
		https://download.docker.com/linux/centos/docker-ce.repo
fi
if ! yum -q repolist enabled | grep 'Docker CE' >/dev/null 2>&1 ; then
	# If it's not enabled, enable the stable repo
	yum-config-manager --enable docker-ce-stable >/dev/null
fi

chkinstallrpms docker-ce docker-ce-cli containerd.io

title "Adding erickson user to docker group"
set -- $(grep erickson /etc/passwd | awk -F: '{ print $1 }')
for i in $* ; do
	usermod -aG docker $i
done

title "Enabling and starting docker service"
systemctl enable docker
systemctl start docker

title "Installing docker Compose"
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
curl -s -L "https://github.com/docker/compose/releases/download/$COMPOSEVERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

if ! chk4rpm chef-workstation ; then
	yum -y -q install https://packages.chef.io/files/stable/chef-workstation/0.2.48/el/7/chef-workstation-0.2.48-1.el6.x86_64.rpm
fi
