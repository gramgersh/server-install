#!/bin/bash
#
# This performs the keychain
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

title "Checking for psychotic repo"
if ! check4rpm psychotic-release ; then
	rpm --import http://wiki.psychotic.ninja/RPM-GPG-KEY-psychotic
	yum -y -q install http://packages.psychotic.ninja/7/base/x86_64/RPMS/psychotic-release-1.0.0-1.el7.psychotic.noarch.rpm ||
		errexit 1 "unable to install psychotic repo"
	COUNT=$(yum-config-manager --disable psychotic | wc -l)
	yumupdate --enablerepo=psychotic
fi

if ! check4rpm keychain ; then
	title "Installing keychain"
	yum -y -q --enablerepo=psychotic install keychain
fi
