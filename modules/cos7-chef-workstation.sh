#!/bin/bash
. ../etc/module-helpers.sh $(basename $0) || exit 1

if ! check4rpm chef-workstation; then
	yum -y -q install https://packages.chef.io/files/stable/chef-workstation/0.2.48/el/7/chef-workstation-0.2.48-1.el6.x86_64.rpm
fi
