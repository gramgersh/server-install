#!/bin/bash
#
# This sets up the cos7-netcore 2.1 environment
#
. ../etc/module-helpers.sh $(basename $0) || exit 1

title "Checking for microsoft repo"
if ! check4rpm packages-microsoft-prod ; then
	rpm --import http://wiki.psychotic.ninja/RPM-GPG-KEY-psychotic
	yum -y -q install https://packages.microsoft.com/config/rhel/7/packages-microsoft-prod.rpm || \
		errexit 1 "unable to install microsoft repo"
	yumupdate
fi

chkinstallrpms aspnetcore-runtime-2.1 epel-release nginx

######################################################################
# This one isn't mandatory
chkinstallrpms certbot

#
# We're going to make nginx look more like the Ubuntu variant.  We
# will have the sites-available directory, then they need to be linked
# in to conf.d
#
title "Checking for nginx sites directories."
test -d $SITESTOP || mkdir -p $SITESTOP || exit 1

#
# Set up the environment for automatic install
# https://peteris.rocks/blog/unattended-installation-of-wordpress-on-ubuntu-server/

# Update the nginx sites.
MYNAME=$(uname -n)
title "Added default to $SITESTOP"
echo "
server {
  listen 80;
  server_name $MYNAME;

  location / {
    proxy_pass         http://localhost:5000;
    proxy_http_version 1.1;
	proxy_set_header   Upgrade $http_upgrade;
	proxy_set_header   Connection keep-alive;
	proxy_set_header   Host $host;
	proxy_cache_bypass $http_upgrade;
	proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
	proxy_set_header   X-Forwarded-Proto $scheme;
  }
}
" > $SITESTOP/$WP_DOMAIN.conf

systemctl restart nginx
