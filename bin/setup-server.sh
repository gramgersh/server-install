#!/bin/bash
#
ORIGARGS="$@"
BRANCH=master
RERUNNING=0
NETWORK_IPADDRS=()
NETWORK_DNS=()
NETWORK_DNSSEARCH=()
NETWORK_IPV6ADDRS=()
NETWORK_IPV6DNS=()
EXTRARPMS=()
while [[ $# -gt 0 ]]; do
	case "$1" in
		--rerunning )   RERUNNING=1 ;;
		--branch | -b ) BRANCH="${2}" ; shift ;;
		--serverspec | -s ) SERVERSPEC="${2}" ; shift;;
		--hostname )    NETWORK_HOSTNAME="${2}" ; shift ;;
		--ipv4 )        NETWORK_IPADDRS+=("${2}") ; shift ;;
		--netmask )     NETWORK_NETMASK="${2}" ; shift ;;
		--gateway )     NETWORK_GATEWAY="${2}" ; shift ;;
		--dns )         NETWORK_DNS+=("${2}") ; shift ;;
		--dns-search )  NETWORK_DNSSEARCH+=("${2}") ; shift ;;
		--ipv6 )        NETWORK_IPV6ADDRS+=("${2}") ; shift ;;
		--ipv6gateway ) NETWORK_IPV6_DEFAULTGW="${2}" ; shift ;;
		--ipv6dns )     NETWORK_IPV6DNS+=("${2}") ; shift ;;
		--extrarpm )    EXTRARPMS+=("${2}") ; shift ;;
	esac
	shift
done
# The remaining args are modules
shift $(( OPTIND - 1 ))
THISDIR=$(cd $(dirname $0) && pwd)
PROG=$(basename $0)
if [[ ! -d $THISDIR/../modules ]]; then
	if [[ "$RERUNNING" = "1" ]]; then
		echo "Downloaded bundle is corrupt."
		exit 1
	fi
	# Download the entire bundle and run it from a temp directory
	TMPDIR=$(mktemp -d /tmp/XXXXXXXX)
	trap "/bin/rm -rf $TMPDIR" 0
	curl -s https://bitbucket.org/gramgersh/server-install/get/${BRANCH}.tar.gz | \
		tar -C $TMPDIR -xzf - --strip-components=1 || exit 1
	cd $TMPDIR/servers || exit 1
	/bin/bash ./$PROG --rerunning $ORIGARGS
	RET=$?
	cd /
	exit $RET
fi

# The base modules to install
MODULES=( authorized_keys cos7-server )
if [[ -n "$SERVERSPEC" ]]; then
	if [[ -f $THISDIR/../servers/$SERVERSPEC ]]; then
		. $THISDIR/../servers/$SERVERSPEC
	elif [[ -f $THISDIR/../servers/$SERVERSPEC.serverspec ]]; then
		. $THISDIR/../servers/$SERVERSPEC.serverspec
	else
		echo "Serverspec ($SERVERSPEC): no such file."
	fi
fi
# Add the ramining command line options to the modules
MODULES+=( "$@" )

# Add the add-rpms module
if (( ${#EXTRARPMS[@]} )) ; then
	MODULES+=( cos7-extrarpms )
fi

cd $THISDIR

declare -A SEEN
for module in "${MODULES[@]}" ; do
	case "$module" in
		*.sh ) : ;;
		* ) module="$module.sh" ;;
	esac
    if [[ -n "${SEEN[$module]}" ]] ; then
		continue
	fi	
	SEEN[$module]=seen
	if [[ -f ../modules/$module ]] ; then
	    /bin/bash ../modules/$module
	else
		echo "${module}: no such module"
		exit 1
	fi
done
